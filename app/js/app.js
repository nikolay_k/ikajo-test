/**
 * Author: Nikolay Kovalenko
 * Date: 17.12.2017
 * Email: nikolay.arkadjevi4@gmail.com
 * */

var IkajoTest = {

    init: function () {

        try {
            this.modulePopap();
            this.toTop();
            this.enableShare('.sharer');
            this.adaptiveSlider();
            this.formModule();
        } catch (err) {
            console.log(err);
        }

    },

    toTop: function () {
        $('#toTop').bind("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('html, body').stop().animate({
                scrollTop: 0
            }, 1000);
            e.preventDefault();
        });

    },

    /* Public Func For Modal Module */
    modulePopap: function () {

        var settings = {
            popapShowed: false,
            overlay: '.test__modal__overlay',
            modal: '.test__modal',
            ftop: '100px'
        };

        if (settings.popapShowed === true && $.cookie('modal').toString() === "showed") {
            return;
        }

        /* Private Func For Show Modal */
        function showModal() {
            if (settings.popapShowed === false) {
                if ($(window).width() < 700) {
                    settings.ftop = '20px';
                }
                $(settings.overlay + ', ' + settings.modal).show();
                $(settings.modal).animate({top: settings.ftop}, 400);
            }

            return true;
        }

        /* Private Func For Close Modal */
        function closeModal() {
            $(settings.overlay).fadeOut(100);
            $(settings.modal).animate({top: settings.ftop, 'opacity': '0'}, 250, function () {
                $(settings.modal).css({'display': 'none', 'opacity': '1'})
            });
        }

        $(document).on('click', '.test__modal-close', function (e) {
            closeModal();
            e.preventDefault();
            e.stopPropagation();
        });

        if (settings.popapShowed === false && ($.cookie('modal') === undefined || $.cookie('modal') === null)) {
            setTimeout(function () {
                $.cookie('modal', 'showed', {expires: 360});
                showModal();
            }, 1500);

            $(window).scroll(
                function () {
                    if ($(document).scrollTop() + window.innerHeight > $('footer').offset().top) {
                        showModal();
                        settings.popapShowed = true;
                    }
                    else {
                        settings.popapShowed = false;
                    }
                }
            );

        }

    },

    /* Public Function For Share Module */
    enableShare: function (sharer) {
        var $sharer = $(sharer);

        var setts = {
            url: document.location.origin + document.location.pathname
        };

        /* Private Func For Open Share Window */
        function openShareWindow(e) {
            window.open(e.href, 'mywin', 'left=20,top=20,width=500,height=400,toolbar=1,resizable=0');
            return false;
        }

        $sharer.find(".facebook")
            .attr("href", "//www.facebook.com/sharer/sharer.php?u=" + setts.url)
            .on("click", function (e) {
                e.preventDefault();
                e.stopPropagation();
                return openShareWindow(this)
            });

        $sharer.find(".google-plus")
            .attr("href", "//plus.google.com/share?url=" + setts.url)
            .on("click", function (e) {
                e.preventDefault();
                e.stopPropagation();
                return openShareWindow(this)
            });

        $.getJSON("https://graph.facebook.com/" + setts.url, function (json) {
            $sharer.find(".facebook .counter").text(json.share.share_count);
        });

    },

    /* Public function init Slider Module */
    adaptiveSlider: function () {
        function initSlider(effect, random, randEff) {
            if (random === true && randEff !== undefined) {
                effect = randEff[Math.floor(Math.random() * randEff.length)];
            }

            $.each(randEff, function (i, v) {
                $(".slider__item").removeClass(v)
            });

            var nextSlide = $('.slider .slider__item.show');

            var classNames = effect + " animated show";
            nextSlide.removeClass(classNames);

            if (nextSlide.next().length) {
                nextSlide.next().addClass(classNames)
            } else {
                $('.slider .slider__item:first').addClass(classNames)
            }
        }

        $.getJSON("/api/slider.json", function (json) {
            var interval = json.interval;
            setInterval(function () {
                if (json.random === true) {
                    initSlider(json.animation, json.random, json.effects);
                } else {
                    initSlider(json.animation);
                }
            }, interval);
        });
    },
    /* Public function for Form Module */

    formModule: function () {
        var form = $('#form-contact');

        var error = {
            "hasError": false,
            "empty": "Поле не може бути пустим.",
            "email": "Неправильний Email",
            "phone": "Тільки цифри",
            "emptyState": false
        };

        $('.phone').mask('(000) 000-00-00',
            {
                placeholder: "(000) 000-00-00",
                onInvalid: function (val, e, f, invalid, options) {
                    $('.control-label.phone-control').text(error.phone).show();
                    $('.control-label.phone-control').closest('.form-group').addClass('has-error');
                }
            });

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email.toLowerCase());
        }

        form.on('submit', function (e) {
            e.preventDefault();

            $(this).find('.form-group').removeClass('has-error');
            $(this).find('.control-label').hide().text();
            $('#succsses').text('');

            $.each($(this).find('.form-control'), function (k, v) {
                if ($(this).val().toString() === "") {
                    $(this).closest('.form-group').addClass('has-error');
                    $(this).closest('.form-group').find('.control-label').text(error.empty).show();

                    error.emptyState = true;
                } else{
                    error.emptyState = false;
                }
            });

            if (error.emptyState) {
                return false;
            }

            if (!validateEmail($('#exampleInputEmail1').val()) && $('#exampleInputEmail1').val().length) {
                $('.email-control').text(error.email).show();
                $('.control-label.email-control').closest('.form-group').addClass('has-error');

                return false;
            }


            $('#succsses').text('Відправлено').css({
                'color': 'green',
                'margin-top': '20px'
            });
            return true;

        });
    }


};


/* Document Ready */
$(document).ready(
    function () {
        IkajoTest.init();
    }
);

/* Window on Scroll */
$(window).scroll(
    function () {

    }
);

/* Window Resize */
$(window).resize(function () {

});

